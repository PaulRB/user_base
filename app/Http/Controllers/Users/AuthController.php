<?php

namespace App\Http\Controllers\Users;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    /**
     * Identificacion de usuarios
     *
     * @param string $email
     * @param string $password
     * @param boolean $remember_me
     * @return void
     */

    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);
        
        try {
            $credentials = request(['email', 'password']);
            $credentials['active'] = 1;
            $credentials['deleted_at'] = null;

            if (!Auth::attempt($credentials)) {
            return response()->api(401,    
                    trans('auth.failed')
                );
            }

            $user = $request->user();
            $tokenResult = $user->createToken(env('PERSONAL_API_TOKEN'));
            $token = $tokenResult->token;
            
            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
            $token->save();

            return response()->api(200,
                trans('api.login_successfully'),
                [
                    'token' => $tokenResult->accessToken,
                    'expires_at'   => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->timestamp,
                    'user'  => User::where('id', $request->user()->id)
                                ->first()
                ]
            );

        } catch (\Illuminate\Database\QueryException $dbe) {
            return response()->api(409,
                [trans('api.error_db_processs'), $dbe->getMessage()]
            );    
        } catch (\Exception $e) {
            return response()->api(409,
                [trans('api.error_processs'), $e->getMessage()]
            );
        }
    }

    /**
     * Cierra la sesion del usuario
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->token()->revoke();
            return response()->api(200,    
                trans('api.logout_successfully')
            );
        } catch (\Exception $e) {
            return response()->api(409,
                [trans('api.error_processs'), $e->getMessage()]
            );
        }
    }



}
