<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\ResponseFactory;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('api', function ($status = 200, $message = "", $data = "") 
        use ($factory) {
            if (is_array($message) && count($message)>0) {
                $message = implode(" ", $message);
            }
            
            return $factory->json([
                'status' => $status,
                'message' => $message,
                'data' => $data
            ], $status);
        });

    }
}
