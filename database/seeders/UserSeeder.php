<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'User Test';
        $user->email = 'user@host.com';
        $user->password = bcrypt('12345678');
        $user->active = true;
        $user->save();
        $user->assignRole('Super Admin');

    }
}
