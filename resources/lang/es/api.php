<?php
/*
 * @File: pi.php
 * @Project: UserBase
 * @Author: Paul Salcedo F.
 * @Date: Tuesday, 15th September 2020 02:29 pm
 * @Email:  paulsalcedo.dev@gmail.com
 * @Last modified by:   Paul Salcedo F.
 * @Last modified time: Tuesday, 15th September 2020 02:05 pm
 * @License: Privativo
 * @Copyright: Copyright (c) 2020 Paul Salcedo F.
 */




return [

    'completed_successfully' => 'Terminado exitosamente',
    'created_user_successfully' => 'Usuario creado exitosamente',
    'updated_user_successfully' => 'Usuario actualizado exitosamente',
    'delete_user_successfully' => 'Usuario eliminado exitosamente',
    'restore_user_successfully' => 'Usuario restaurado exitosamente',
    'activated_user_successfully' => 'Terminado exitosamente',
    'login_successfully' => 'Login exitosamente',
    'logout_successfully' => 'Logout exitosamente',

    'created_item_successfully' => 'Registro creado exitosamente',
    'updated_item_successfully' => 'Registro actualizado exitosamente',
    'delete_item_successfully' => 'Registro eliminado exitosamente',
    'restore_item_successfully' => 'Registro restaurado exitosamente',
    
    // 
    'unauthorized' => 'No autorizado para acceder al recurso',
    'not_allowed' => 'Metodo no permitido',

    // Errors
    'error_processs' => 'Error ejecuntado el metodo solicitado',
    'error_db_processs' => 'Error ejecuntado sentencia en la DB',
    'error_invalid_token' => 'El token es inválido',

    'error_missing_parameters' => 'Faltan parametros para procesar',

];
